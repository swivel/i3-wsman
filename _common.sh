#!/usr/bin/env bash

I3WSM_GET_WORKSPACES=$(i3-msg -t get_workspaces)

I3WSM_AW=$(echo "$I3WSM_GET_WORKSPACES" | jq '.[] | select(.focused==true)')
#I3WSM_AW_NUM=$(echo "$I3WSM_AW" | jq '.num')
#I3WSM_AW_NAME=$(echo "$I3WSM_AW" | jq -r '.name')

I3WSM_AO=$(echo "$I3WSM_AW" | jq -r '.output')
I3WSM_AO_WS=$(echo "$I3WSM_GET_WORKSPACES" | jq -r 'map(select(.output=="'$I3WSM_AO'"))|sort_by(.num)')
#I3WSM_AO_LAST_WS=$(echo "$I3WSM_AO" | jq -r '.[-1]')
#I3WSM_AO_LAST_WS_NUM=$(echo "$I3WSM_AO_WS" | jq -r '.[-1].num')
#I3WSM_AO_LAST_WS_NAME=$(echo "$I3WSM_AO_WS" | jq -r '.[-1].name')
#I3WSM_AO_LAST_WS_NAME=$(echo "$I3WSM_AO_LAST_WS" | jq -r '.name')

#I3WSM_ALL_WS_NUMS=$(echo "$I3WSM_GET_WORKSPACES" | jq '.[].num')
#I3WSM_ALL_LAST_WS_NUM=$(echo "$I3WSM_GET_WORKSPACES" | jq 'sort_by(.num)|.[-1].num')



# Unique Session ID - Useful for disambiguating the current user session
# @return {string} A unique string to help represent the current user session
i3wsm::usid() {
	echo "${XDG_SESSION_ID}${XDG_VTNR}${XDG_SEAT}"
}


# Function to create a temporary file
# @param {string} filename - The name of the temporary file
# @return {string} The absolute path of the created temporary file
i3wsm::create_tempf() {
	local filename="i3wsm::tmp_$(i3wsm::usid)_${1}"
	local temp_dir

	# Check if /dev/shm is supported
	if [[ -d "/dev/shm" ]]; then
		temp_dir="/dev/shm"
	else
		temp_dir="/tmp"
	fi

	# Create the directory if it doesn't exist
	mkdir -p "$temp_dir"

	# Determine the absolute path of the temporary file
	local temp_file="$temp_dir/$filename"

	# Touch the file if it doesn't exist
	#if [[ ! -e "$temp_file" ]]; then
	#	touch "$temp_file"
	#fi

	echo "$temp_file"
}


# Grabs the Nth workspace on the current output
# @param {number} n - The position of the workspace on the output
# @return {json} The workspace object
i3wsm::get_nth_on_ao() {
	[[ "$1" == "0" ]] && echo -n "null" || {
		ao_ws=$(echo "$I3WSM_GET_WORKSPACES" | jq -r 'map(select(.output=="'$I3WSM_AO'")) | sort_by(.num)')
		echo "$ao_ws" | jq ".[$1-1]"
	}
}


# Function to rename workspaces
# @param {string} from - The current workspace name
# @param {string} to - The new workspace name
i3wsm::swap_workspaces() {
	i3-msg rename workspace "$1" to "swapinprogress:$1"
	i3-msg rename workspace "$3" to "$4"
	i3-msg rename workspace "swapinprogress:$1" to "$2"
}

# Parses the name of the workspace
# @param {string} name - Name of the workspace, like "1" or "1:foo"
# @returns {string} Returns the proper name, like "1" or "foo"
i3wsm::parse_name() {
	echo "${1#*:}"
}

# Parses the name of the workspace
# @param {string} name - Name of the workspace, like "1" or "1:foo"
# @returns {string} Returns the proper name, like "1" or "foo"
i3wsm::change_prefix() {
	prefix="${1%%:*}"
	suffix=$(i3wsm::parse_name $1)
	if [ "$suffix" != "$prefix" ]; then
		echo "$2:$suffix"
	else
		echo "$2"
	fi
}

# Moved the specified workspace to the right
# @param {number} ws_num - The workspace num to move
i3wsm::move_ws_right() {
	local ws ws_num ws_num_new ws_name ws_neighbor

	ws_num="$1"
	ws_num_new=$(($ws_num + 1))
	ws=$(echo "$I3WSM_GET_WORKSPACES" | jq ".[] | select(.num==$ws_num)")
	if [ ! -z "$ws" ] && [ "$ws" != "null" ]; then
		ws_neighbor=$(i3wsm::get_immediate_neighbor "$ws_num" "right")
		if [ ! -z "$ws_neighbor" ] && [ "$ws_neighbor" != "null" ]; then
			ws_neighbor_num=$(echo "$ws_neighbor" | jq -r '.num')
			if [[ "$ws_num_new" == "$ws_neighbor_num" ]]; then
				i3wsm::move_ws_right "$ws_neighbor_num"
			fi
		fi

		ws_name=$(echo "$ws" | jq -r '.name')
		ws_name_new=$(i3wsm::change_prefix "$ws_name" "$ws_num_new")

		i3-msg rename workspace "$ws_name" to "$ws_name_new"
	fi
}

# Retrieves the neighbor workspace object in the given direction ("left" or "right").
# @param {number} current_num - The workspace number to find the neighbor for.
# @param {string} direction - The direction to find the neighbor workspace ("left" or "right").
# @returns {object} Returns the neighbor workspace object.
i3wsm::get_immediate_neighbor() {
	local current_num="$1"
	local direction="$2"

	local adjacent_num

	if [ "$direction" == "left" ]; then
		adjacent_num=$(($current_num - 1))
	elif [ "$direction" == "right" ]; then
		adjacent_num=$(($current_num + 1))
	fi

	echo "$I3WSM_GET_WORKSPACES" | jq -r ".[] | select(.num==$adjacent_num)"
}

# Retrieves the neighbor workspace object in the given direction ("left" or "right").
# @param {number} current_num - The workspace number to find the neighbor for.
# @param {string} direction - The direction to find the neighbor workspace ("left" or "right").
# @returns {object} Returns the neighbor workspace object.
i3wsm::get_neighbor() {
	local current_num="$1"
	local direction="$2"

	local current_output
	local output_workspaces
	local output_ws_nums
	local adjacent_num

	current_ws=$(echo "$I3WSM_GET_WORKSPACES" | jq ".[] | select(.num==$current_num)")
	current_output=$(echo "$current_ws" | jq -r '.output')

	output_workspaces=$(echo "$I3WSM_GET_WORKSPACES" | jq -r 'map(select(.output=="'$current_output'"))|sort_by(.num)')
	output_ws_nums=$(echo "$output_workspaces" | jq "[.[] | .num]")

	if [ "$direction" == "left" ]; then
		adjacent_num=$(echo "$output_ws_nums" | jq "[.[] | select(. < $current_num)] | max")
	elif [ "$direction" == "right" ]; then
		adjacent_num=$(echo "$output_ws_nums" | jq "[.[] | select(. > $current_num)] | min")
	else
		echo -n "null"
		exit 0
	fi

	echo "$I3WSM_GET_WORKSPACES" | jq -r ".[] | select(.num==$adjacent_num)"
}

# Reorders the specified workspace in the given direction ("left" or "right").
# @param {number} active_ws_num - The active workspace number to reorder.
# @param {string} direction - The direction to reorder the workspace ("left" or "right").
i3wsm::reorder_ws() {
	local direction
	local current_ws current_num current_name current_name_new current_suffix
	local adjacent_ws adjacent_num adjacent_name adjacent_name_new adjacent_suffix

	direction="$2"

	current_num="$1"
	current_ws=$(echo "$I3WSM_GET_WORKSPACES" | jq ".[] | select(.num==$current_num)")
	[ -z "$current_ws" ] || [ "$current_ws" == "null" ] && exit 0
	current_name=$(echo "$current_ws" | jq -r '.name')

	adjacent_ws=$(i3wsm::get_neighbor "$current_num" "$direction")
	[ -z "$adjacent_ws" ] || [ "$adjacent_ws" == "null" ] && exit 0
	adjacent_num=$(echo "$adjacent_ws" | jq -r ".num")
	adjacent_name=$(echo "$adjacent_ws" | jq -r ".name")

	adjacent_name_new=$(i3wsm::change_prefix "$adjacent_name" "$current_num")
	current_name_new=$(i3wsm::change_prefix "$current_name" "$adjacent_num")

	if [ ! -z "$adjacent_name" ] && [ "$adjacent_name" != "null" ]; then
		i3wsm::swap_workspaces "$current_name" "$current_name_new" "$adjacent_name" "$adjacent_name_new"
	else
		echo "Active workspace is already the rightmost on this output."
	fi
}


TEST_i3wsm::common() {
	# TEST: Global Vars
	echo -e "Global Vars\n========"
	echo I3WSM_GET_WORKSPACES=$I3WSM_GET_WORKSPACES
	echo I3WSM_GET_WORKSPACE_NAMES=$(echo "$I3WSM_GET_WORKSPACES" | jq 'sort_by(.num) | .[] | .name')
	echo I3WSM_GET_WORKSPACE_NUMS=$(echo "$I3WSM_GET_WORKSPACES" | jq 'sort_by(.num) | .[] | .num')
	echo I3WSM_AW=$I3WSM_AW
	echo I3WSM_AW_NUM=$(echo "$I3WSM_AW" | jq '.num')
	echo I3WSM_AW_NAME=$(echo "$I3WSM_AW" | jq -r '.name')
	echo I3WSM_AO=$I3WSM_AO
	I3WSM_AO_WS=$(echo "$I3WSM_GET_WORKSPACES" | jq -r 'map(select(.output=="'$I3WSM_AO'")) | sort_by(.num)')
	echo I3WSM_AO_WS=$I3WSM_AO_WS
	I3WSM_AO_LAST_WS=$(echo "$I3WSM_AO_WS" | jq -r '.[-1]')
	echo I3WSM_AO_LAST_WS=$I3WSM_AO_LAST_WS
	echo I3WSM_AO_LAST_WS_NUM=$(echo "$I3WSM_AO_LAST_WS" | jq -r '.num')
	echo I3WSM_AO_LAST_WS_NAME=$(echo "$I3WSM_AO_LAST_WS" | jq -r '.name')
	echo ""

	# TEST: i3wsm::get_nth_on_ao
	echo -e "i3wsm::get_nth_on_ao\n========"
	echo "0 on AO: $(i3wsm::get_nth_on_ao 0)"
	echo "1 on AO: $(i3wsm::get_nth_on_ao 1)"
	echo "2 on AO: $(i3wsm::get_nth_on_ao 2)"
	echo "10 on AO: $(i3wsm::get_nth_on_ao 10)"
	echo ""

	# TEST: i3wsm::create_tempf
	echo -e "i3wsm::create_tempf\n========"
	local I3WSMTEST_temp_file=$(i3wsm::create_tempf "my_temp_file.txt")
	echo "Temporary file path: $I3WSMTEST_temp_file"
	echo ""
}

[[ "${BASH_SOURCE[0]}" == "$0" ]] && TEST_i3wsm::common
