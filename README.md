# i3-wsman

i3-wsman is a collection of bash scripts designed to manage workspaces in the i3 window manager. It provides various functionalities to navigate, reorder, and manipulate workspaces.

## Dependencies

- i3 window manager
- jq (for JSON processing)

## Installation

Clone the repository and make sure to add the scripts to your PATH or link them to a directory in your PATH.

### Example i3 config:
```
# switch to workspace
bindsym $mod+1 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 1 2>~/bin/i3/err.log"
bindsym $mod+2 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 2 2>~/bin/i3/err.log"
bindsym $mod+3 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 3 2>~/bin/i3/err.log"
bindsym $mod+4 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 4 2>~/bin/i3/err.log"
bindsym $mod+5 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 5 2>~/bin/i3/err.log"
bindsym $mod+6 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 6 2>~/bin/i3/err.log"
bindsym $mod+7 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 7 2>~/bin/i3/err.log"
bindsym $mod+8 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 8 2>~/bin/i3/err.log"
bindsym $mod+9 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 9 2>~/bin/i3/err.log"
bindsym $mod+0 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-goto 10 2>~/bin/i3/err.log"

# move focused container to workspace
bindsym $mod+Shift+1 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 1 2>~/bin/i3/err.log"
bindsym $mod+Shift+2 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 2 2>~/bin/i3/err.log"
bindsym $mod+Shift+3 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 3 2>~/bin/i3/err.log"
bindsym $mod+Shift+4 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 4 2>~/bin/i3/err.log"
bindsym $mod+Shift+5 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 5 2>~/bin/i3/err.log"
bindsym $mod+Shift+6 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 6 2>~/bin/i3/err.log"
bindsym $mod+Shift+7 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 7 2>~/bin/i3/err.log"
bindsym $mod+Shift+8 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 8 2>~/bin/i3/err.log"
bindsym $mod+Shift+9 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 9 2>~/bin/i3/err.log"
bindsym $mod+Shift+0 exec --no-startup-id "~/bin/i3-wsman/i3-wsm-move-to 10 2>~/bin/i3/err.log"

bindsym $mod+Ctrl+h exec --no-startup-id "~/bin/i3-wsman/i3-wsm-prev 2>~/bin/i3/err.log"
bindsym $mod+Ctrl+l exec --no-startup-id "~/bin/i3-wsman/i3-wsm-next 2>~/bin/i3/err.log"

bindsym $mod+Shift+Ctrl+h exec --no-startup-id "~/bin/i3-wsman/i3-wsm-reorder left 2>~/bin/i3/err.log"
bindsym $mod+Shift+Ctrl+l exec --no-startup-id "~/bin/i3-wsman/i3-wsm-reorder right 2>~/bin/i3/err.log"

bindsym $mod+Mod1+h exec --no-startup-id "~/bin/i3-wsman/i3-wsm-adjacent left 2>~/bin/i3/err.log"
bindsym $mod+Mod1+l exec --no-startup-id "~/bin/i3-wsman/i3-wsm-adjacent right 2>~/bin/i3/err.log"
```

## Scripts

### `i3-wsm-prev`
Navigate to the previous workspace. Does not loop.

### `i3-wsm-next`
Navigate to the next workspace. If the workspace does not exist, it will create a new one.

### `i3-wsm-reorder`
Move the current workspace to the left or right.

### `i3-wsm-adjacent`
Creates a workspace adjacent to the active workspace in a given direction (left or right).

### `i3-wsm-goto`
Navigate to the Nth workspace on the active output. If the workspace does not exist, it will navigate to the last workspace.

### `i3-wsm-move-to`
Move the current window to the Nth workspace on the active output. If the workspace does not exist, it will create a new one.

## Contribution

Feel free to contribute by opening issues or submitting pull requests.

